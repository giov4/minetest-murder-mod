local function register_hand_from_texture(texture)
	local hand_name = "murder:hand_" .. tostring(texture:gsub(".png", ""))
	local hand_def = {}

	-- costruisco la definizione per una nuova mano prendendo le proprietà
	-- che non mi interessano e impostandole ai valori di base
	for key, value in pairs(minetest.registered_items[""]) do
		if key ~= "mod_origin" and key ~= "type" and key ~= "wield_image" then
			hand_def[key] = value
		end
	end

	hand_def.tiles = {texture}
	hand_def.visual_scale = 1
	hand_def.wield_scale = {x=4,y=4.5,z=4.5}
	hand_def.paramtype = "light"
	hand_def.drawtype = "mesh"
	hand_def.mesh = "murder_hand.obj"
	hand_def.use_texture_alpha = "clip"

	minetest.register_node(hand_name, hand_def)
end



for i, texture in ipairs(murder_settings.skins) do
	register_hand_from_texture(texture)
end