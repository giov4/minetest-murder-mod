local S = minetest.get_translator("murder")

-- Registering the murder_admin privilege.
minetest.register_privilege("murder_admin", {
    description = S("It allows you to use /murder")
})



local cmd = chatcmdbuilder.register("murder", {
  description =
    "\n\n"..
    "- logs <"..S("arena name")..">\n",
  privs = { murder_admin = true }
})

-- Debug commands:
cmd:sub("play :sound :gain:number", function(pl_name, sound, gain)
    minetest.sound_play(sound, { pos = minetest.get_player_by_name(pl_name):get_pos(), gain = gain})
end)



cmd:sub("logs :arena", function(pl_name, arena)
    murder.print_logs(arena, pl_name)
end)