local GRAVITY = 9.81

-- Spawns particles of a breaking bomb node at pos
function bomb_particles(pos)
	minetest.add_particlespawner({
		amount = 24,
		time = 0.01,
		pos = {
			min = vector.add(pos, vector.new(-0.3, -0.3, -0.3)),
			max = vector.add(pos, vector.new(0.3, 0.3, 0.3)),
		},
		vel = {
			min = vector.new(-5.5, -5.5, -5.5),
			max = vector.new(5.5, 5.5, 5.5),
		},
		acc = vector.new(0, -GRAVITY, 0),
		exptime = { min = 0.5, max = 1.0 },
		size = { min = 1.5, max = 2.0 },
		collisiondetection = true,
		vertical = false,
		node = {name="murder:bomb"},
	})
end

-- Spawns explosion particles at pos
function explosion_particles(pos)
	local radius = 5
	minetest.add_particlespawner({
		amount = 128,
		time = 0.6,
		pos = {
			min = vector.subtract(pos, radius / 2),
			max = vector.add(pos, radius / 2),
		},
		vel = {
			min = vector.new(-20, -20, -20),
			max = vector.new(20, 20, 20),
		},
		acc = vector.zero(),
		exptime = { min = 0.2, max = 2.0 },
		size = { min = 16, max = 24 },
		drag = vector.new(1,1,1),
		texture = {
			name = "murder_smoke_anim_1.png", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
			name = "murder_smoke_anim_2.png", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
			name = "murder_smoke_anim_1.png^[transformFX", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
			name = "murder_smoke_anim_2.png^[transformFX", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = -1 },
		},
	})
	minetest.add_particlespawner({
		amount = 1,
		time = 0.01,
		pos = pos,
		vel = vector.zero(),
		acc = vector.zero(),
		exptime = 2,
		size = radius*10,
		texture = "murder_smoke_ball_big.png",
		animation = { type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = -1, },
	})
	minetest.add_particlespawner({
		amount = 4,
		time = 0.25,
		pos = pos,
		vel = vector.zero(),
		acc = vector.zero(),
		exptime = { min = 1.2, max = 1.8 },
		size = { min = 8, max = 12 },
		radius = { min = 0.5, max = math.max(0.6, radius*0.75) },
		texture = "murder_smoke_ball_medium.png",
		animation = { type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = -1, },
	})
	minetest.add_particlespawner({
		amount = 28,
		time = 0.5,
		pos = pos,
		vel = vector.zero(),
		acc = vector.zero(),
		exptime = { min = 1.0, max = 1.6 },
		size = { min = 6, max = 8 },
		radius = { min = 1, max = math.max(1.1, radius+1) },
		texture = "murder_smoke_ball_small.png",
		animation = { type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = -1, },
	})
end



function murder.remove_bomb(arena)
    if arena.emergency_data and arena.emergency_data.bomb_pos then 
        local node = minetest.get_node(arena.emergency_data.bomb_pos)
        if node.name == "murder:bomb" then
            minetest.set_node(arena.emergency_data.bomb_pos, {name = "air"})
        end
    end

    -- Removing the waypoint and stopping the sound for each player.
    for pl_name, _ in pairs(arena.players_and_spectators) do
        murder.remove_hud(pl_name, "waypoint-emergency")
        murder.stop_sound(pl_name, "murder-emergency")
    end
end



function murder.place_bomb(arena, pl_name)
    local murderer = arena.roles[pl_name]
    local detonation_time = murderer.bomb_detonation_time
    local waypoint_emergency = {
        hud_elem_type = "image_waypoint",
        world_pos = murderer.bomb_pos,
        text      = "HUD_murder_emergency.png",
        scale     = {x = 5, y = 5},
        size = {x = 200, y = 200},
    }

    minetest.set_node(murderer.bomb_pos, {name = "murder:bomb"})
    minetest.get_meta(murderer.bomb_pos):set_string("match_id", tostring(arena.match_id))
    murderer.bomb_detonated = true
    arena.emergency_data.bomb_pos = murderer.bomb_pos
    arena.emergency_data.murderer = murderer
    local original_match_id = arena.match_id

    -- Adding a waypoint and playing music to each player and spectator.
    for pl_name, _ in pairs(arena.players_and_spectators) do
        murder.add_hud(pl_name, "waypoint-emergency", waypoint_emergency)
        murder.add_sound(pl_name, "murder-emergency", {to_player = pl_name, loop = true})
    end

    -- Bomb explosion.
    minetest.after(detonation_time, function()
        if arena.in_game and not arena.in_celebration and original_match_id == arena.match_id and murderer.bomb_detonated then
            for pl_name, _ in pairs(arena.players_and_spectators) do
                minetest.sound_play("murder-explosion", {to_player = pl_name})
            end
            murder.remove_bomb(arena)
            if arena.emergency_data and arena.emergency_data.bomb_pos then
                bomb_particles(arena.emergency_data.bomb_pos)
                local boompos = vector.add(arena.emergency_data.bomb_pos, vector.new(0,0.25,0))
                explosion_particles(boompos)
            end
            murder.player_wins(pl_name)
        end
    end)
end
